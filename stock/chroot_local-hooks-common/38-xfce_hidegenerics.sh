#!/bin/sh
# Prevent the main menu from being bafflingly cluttered by duplicates, introduced by "exo" generic launchers

for f in exo-web-browser exo-mail-reader exo-file-manager exo-terminal-emulator xfce4-session-logout
do
	fullf="/usr/share/applications/$f.desktop"
	if [ -e "$fullf" ]
	then
		echo "NoDisplay=true" >> "$fullf"
	fi
done

