#!/bin/sh
# Most of the time we use the live-initramfs from git and install it as a
# local_package, so update-initramfs is needed to create a new initrd

update-initramfs -k $(/bin/ls /lib/modules) -c -t
