#!/bin/bash
# VLC on live boot fails with "can't find plugins" -- gets confused by squashfs -- we need to specify the plugin path

if [ -e "/usr/share/applications/vlc.desktop" ]
then
    echo "51c51
< Exec=vlc %U
---
> Exec=vlc --plugin-path=/usr/lib/vlc/plugins %U" | patch --no-backup-if-mismatch --forward /usr/share/applications/vlc.desktop
fi
